package org.academiadecodigo.tailormoons.wordscrambler.scrambler;

public class Timer {

    private long initTime;
    private int duration;
    private int seconds;


    public boolean timeUp() {
        return (System.currentTimeMillis() / 1000) > initTime + duration;
    }

    public boolean halfTime() {
        return (System.currentTimeMillis() / 1000) >= ((initTime + (duration / 2)));
    }

    public void start() {
        seconds = 30;
        this.initTime = System.currentTimeMillis() / 1000;
        this.duration = seconds;
    }

    public int getSeconds() {
        return seconds;
    }
}



package org.academiadecodigo.tailormoons.wordscrambler.scrambler.commands;

import org.academiadecodigo.tailormoons.wordscrambler.scrambler.Scrambler;
import org.academiadecodigo.tailormoons.wordscrambler.server.ClientHandler;
import org.academiadecodigo.tailormoons.wordscrambler.server.Server;

public class StatusHandler implements ScrambleCommandsHandler {

    private ClientHandler clientHandler;
    private Server server;
    private String message;
    private Scrambler scrambler;


    @Override
    public void execute(ClientHandler clientHandler, Server server, String message, Scrambler scrambler) {
        this.clientHandler = clientHandler;
        this.server = server;
        this.message = message;
        this.scrambler = scrambler;

        String[] words = message.split(" ");
        String command = words[0].toLowerCase();

        if (command.equals("/status")) {
            if (words.length == 1) {

                server.alert("--------------------------------------------------------------------\nGAME STATUS:", clientHandler);
                for (ClientHandler player : scrambler.getPlayers()) {
                    if (player != clientHandler) {
                        if (scrambler.getPlayers().contains(player)) {
                            server.alert(player.getName() + " has now " + player.getScore() +
                                    " points. And has already scrambled one time in this round.", clientHandler);
                        }
                        else if (!scrambler.getPlayers().contains(player)) {
                            server.alert(player.getName() + " has now " + player.getScore() +
                                    " points. And has not scrambled in this round.", clientHandler);
                        }
                    }
                }
                if (scrambler.getPlayers().contains(clientHandler)) {
                    server.alert("You have currently " + clientHandler.getScore() +
                            " points. You have also already scrambled one time in this round.", clientHandler);
                }
                else if (!scrambler.getPlayers().contains(clientHandler)) {
                    server.alert("You have currently " + clientHandler.getScore() +
                            " points. You have not picked a word to scramble.\nGo make a scramble!", clientHandler);
                }
                server.alert("--------------------------------------------------------------------", clientHandler);

            } else {
                server.alert("Use '/help' to get a list of all available commands.", clientHandler);
            }
        }
    }
}


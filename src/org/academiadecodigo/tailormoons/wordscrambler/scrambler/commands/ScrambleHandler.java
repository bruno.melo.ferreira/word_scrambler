package org.academiadecodigo.tailormoons.wordscrambler.scrambler.commands;

import org.academiadecodigo.tailormoons.wordscrambler.scrambler.Scrambler;
import org.academiadecodigo.tailormoons.wordscrambler.server.ClientHandler;
import org.academiadecodigo.tailormoons.wordscrambler.server.Server;

public class ScrambleHandler implements ScrambleCommandsHandler {

    private ClientHandler clientHandler;
    private Server server;
    private String message;
    private Scrambler scrambler;


    @Override
    public void execute(ClientHandler clientHandler, Server server, String message, Scrambler scrambler) {
        this.clientHandler = clientHandler;
        this.server = server;
        this.message = message;
        this.scrambler = scrambler;

        String[] words = message.split(" ");
        String command = words[0].toLowerCase();


        if (command.equals("/scramble") && !scrambler.getPlayers().contains(clientHandler)) {

            if (words.length == 2 && server.getScrambler().getWordToScramble() == null) {
                String wordToScramble = words[1].toLowerCase();
                String wordArray[] = wordToScramble.split("");
                char[] splitedWord = words[1].toLowerCase().toCharArray();

                int counter = 1;

                for (int i = 0; i < splitedWord.length - 1; i++) {
                    if (splitedWord[0] == splitedWord[i + 1]) {
                        counter += 1;
                    }
                }

                if (wordArray.length <= 3) {
                    server.alert(("Pick a bigger word, please " + clientHandler.getName() + ". Minimum of 3 characters."), clientHandler);

                } else if (wordArray.length >= 10) {
                    server.alert(("Pick a smaller word, please " + clientHandler.getName() + ". Maximum of 10 characters."), clientHandler);

                } else if (!checkLetters(wordToScramble)) {
                    server.alert("No numbers or special characters allowed. Choose another word, please.", clientHandler);

                } else if (counter == splitedWord.length) {
                    server.alert("You can´t choose this word, all letters are the same. Please choose another one.", clientHandler);

                } else if (scrambler.getRoundWords().contains(wordToScramble)) {
                    server.alert("Repeated word. Please choose another one.", clientHandler);

                } else {
                    scrambler.scramble(wordToScramble, clientHandler);

                    if (scrambler.getNumberOfRounds() == 1) {
                        server.alert("You've just picked a new word to scramble.", clientHandler);
                        server.getTimer().start();
                        scrambler.getPlayers().add(scrambler.getCurrentPlayer());//eloi
                        server.broadcast(("\n" + clientHandler.getName() + " started a new game.\n" +
                                "Use '/guess <word>' to try to guess the word!\n" +
                                "You have " + server.getTimer().getSeconds() + " seconds to find out the word.\n"), clientHandler);
                        server.broadcast("Scrambled word is: " + server.getScrambler().getScrambledWord(), clientHandler);

                    } else if (scrambler.getNumberOfRounds() > 1) {
                        server.alert("You've just picked a new word to scramble. Round " + scrambler.getNumberOfRounds() + " just began!" + "\n", clientHandler);
                        server.getTimer().start();
                        scrambler.getPlayers().add(scrambler.getCurrentPlayer());//eloi
                        server.broadcast((clientHandler.getName() + " started a new round.\n" +
                                "Use '/guess <word>' to try to guess the word!\n" +
                                "You have " + server.getTimer().getSeconds() + " seconds to find out the word.\n"), clientHandler);
                        server.broadcast("Scrambled word is: " + server.getScrambler().getScrambledWord(), clientHandler);
                    }
                }

            } else if (server.getScrambler().getWordToScramble() != null) {
                server.alert("A WordScrambler game has already started, please try to guess the scrambled word first!", clientHandler);

            } else if (words.length > 2) {
                server.alert("You can only enter one word to scramble.", clientHandler);

            } else {
                server.alert("You have to write the desired word after the command: '/scramble <word>'.", clientHandler);
            }

        } else if (scrambler.getPlayers().size() == server.getList().size()) {

            scrambler.increaseNumberOfRounds(1);
            scrambler.getPlayers().clear();
            server.broadcast("\nROUND " + scrambler.getNumberOfRounds(), clientHandler);
            execute(clientHandler, server, message, scrambler);

        } else if (scrambler.getPlayers().contains(clientHandler)) {
            server.alert("You have already picked a word. Let the other players choose a word.", clientHandler);
        }
    }

    public boolean checkLetters(String string) {
        return string.matches("[a-zA-Z]+");
    }

}

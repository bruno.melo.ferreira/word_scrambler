package org.academiadecodigo.tailormoons.wordscrambler.scrambler.commands;

import org.academiadecodigo.tailormoons.wordscrambler.scrambler.Scrambler;
import org.academiadecodigo.tailormoons.wordscrambler.server.ClientHandler;
import org.academiadecodigo.tailormoons.wordscrambler.server.Server;

public interface ScrambleCommandsHandler {

    void execute(ClientHandler clientHandler, Server server, String message, Scrambler scrambler);

}

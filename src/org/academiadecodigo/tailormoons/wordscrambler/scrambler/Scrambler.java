package org.academiadecodigo.tailormoons.wordscrambler.scrambler;

import org.academiadecodigo.tailormoons.wordscrambler.server.ClientHandler;

import java.util.LinkedList;

public class Scrambler {

    private String wordToScramble;
    private String scrambledWord;
    private LinkedList<String> roundWords = new LinkedList<>();
    private ClientHandler currentPlayer;
    private boolean doubleRound;
    private LinkedList<ClientHandler> players = new LinkedList<ClientHandler>();
    private int numberOfRounds = 1;


    public String scramble(String word, ClientHandler currentPlayer) {
        this.currentPlayer = currentPlayer;

        String[] wordArray = word.split("");
        String scrambledString = "";
        wordToScramble = word;


        for (int i = 0; i < wordArray.length; i++) {

            int randomIndexToSwap = (int) (Math.random() * wordArray.length);

            String temp = wordArray[randomIndexToSwap];
            wordArray[randomIndexToSwap] = wordArray[i];
            wordArray[i] = temp;
        }

        for (String letter : wordArray) {
            scrambledString = scrambledString + letter;
        }

        if (scrambledString.equals(word)) {
            System.out.println(scrambledString);
            scramble(word, currentPlayer);
        } else {
            scrambledWord = scrambledString;
            roundWords.add(wordToScramble);
        }
        return scrambledString;
    }



    public String getWordToScramble() {
        return wordToScramble;
    }

    public void setWordToScrambleToNull() {
        wordToScramble = null;
    }

    public String getScrambledWord() {
        return scrambledWord;
    }

    public LinkedList<String> getRoundWords() {
        return roundWords;
    }

    public void resetList() {
        roundWords.clear();
    }

    public ClientHandler getCurrentPlayer() {
        return currentPlayer;
    }

    public void setDoubleRound(boolean doubleRound) {
        this.doubleRound = doubleRound;
    }

    public boolean isDoubleRound() {
        return doubleRound;
    }

    public LinkedList<ClientHandler> getPlayers() {
        return players;
    }

    public int getNumberOfRounds() {
        return numberOfRounds;
    }

    public void increaseNumberOfRounds(int round) {
        this.numberOfRounds += round;
    }
}

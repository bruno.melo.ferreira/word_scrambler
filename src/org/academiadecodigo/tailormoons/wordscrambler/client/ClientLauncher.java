package org.academiadecodigo.tailormoons.wordscrambler.client;

import java.io.IOException;

public class ClientLauncher {
    public static void main(String[] args) {

        Client client = new Client();

        try {
            client.start();
        } catch (IOException e) {
            System.out.println("Error opening Client.");
        }
    }
}

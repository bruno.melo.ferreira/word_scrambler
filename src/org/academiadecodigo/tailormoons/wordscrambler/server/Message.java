package org.academiadecodigo.tailormoons.wordscrambler.server;

import java.io.*;

public class Message {

    public void sendMessages(String file) {

        BufferedReader in;
        try {
            in = new BufferedReader(new InputStreamReader(new FileInputStream("assets/" + file)));
            String line;
            try {
                while ((line = in.readLine()) != null) {
                    System.out.println(line);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (
                FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}

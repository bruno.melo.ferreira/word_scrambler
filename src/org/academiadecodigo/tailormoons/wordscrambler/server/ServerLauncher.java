package org.academiadecodigo.tailormoons.wordscrambler.server;

public class ServerLauncher {

    public static final int DEFAULT_PORT = 8080;

    public static void main(String[] args) {
        int port = DEFAULT_PORT;

        if (args.length > 0) {
            port = Integer.parseInt(args[0]);
        }

        Server server = new Server(port);
        server.run();
        
    }
}

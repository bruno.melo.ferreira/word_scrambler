package org.academiadecodigo.tailormoons.wordscrambler.server;

import org.academiadecodigo.tailormoons.wordscrambler.scrambler.commands.*;
import org.academiadecodigo.tailormoons.wordscrambler.server.commands.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

public class ClientHandler implements Runnable {

    private Socket client;
    private PrintWriter out;
    private BufferedReader in;
    private Server server;
    private String name;
    private Map<String, CommandHandler> commandHandlers;
    private Map<String, ScrambleCommandsHandler> scrambleCommandsHandler;
    private int score;

    public ClientHandler(Socket client, Server server) {
        this.client = client;
        this.server = server;
        score = 0;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Socket getSocket() {
        return client;
    }


    @Override
    public void run() {

        init();
        getNameAtLogin();
        server.broadcast(name + " just joined the game!", this);


        while (!client.isClosed()) {

            try {
                out = new PrintWriter(client.getOutputStream(), true);
                in = new BufferedReader(new InputStreamReader(client.getInputStream()));
            } catch (IOException exception) {
                exception.printStackTrace();
            }

            try {
                String messagePrefix = name + ": ";
                String message = in.readLine();

                if (message == null) {
                    client.close();
                    continue;
                }
                if (message.isEmpty() || message.isBlank()) {
                    server.alert("You cannot send blank messages.", this);
                    continue;
                }
                message = message.trim();
                if (!message.startsWith("/")) {
                    server.broadcast(messagePrefix + message, this);
                } else {
                    checkForCommand(message);
                }
            } catch (IOException exception) {
                exception.printStackTrace();
            }

        }
    }

    private void init() {
        commandHandlers = new HashMap<>();
        commandHandlers.put("/name", new ChangeNameHandler());
        commandHandlers.put("/list", new OnlineListHandler());
        commandHandlers.put("/logout", new LogoutHandler());
        commandHandlers.put("/whisper", new WhisperHandler());
        commandHandlers.put("/", new SlashUseHandler());

        scrambleCommandsHandler = new HashMap<>();
        scrambleCommandsHandler.put("/help", new HelpHandler());
        scrambleCommandsHandler.put("/scramble", new ScrambleHandler());
        scrambleCommandsHandler.put("/guess", new GuessHandler());
        scrambleCommandsHandler.put("/status", new StatusHandler());

    }

    public void checkForCommand(String message) {
        String[] words = message.split(" ");
        String command = words[0].toLowerCase();

        if (commandHandlers.containsKey(command)) {
            commandHandlers.get(command).execute(this, server, message);
        }
        else if (scrambleCommandsHandler.containsKey(command)) {
            scrambleCommandsHandler.get(command).execute(this, server, message, server.getScrambler());
        }
        else {
            server.alert("'" + command + "'" + " is not a command.", this);
        }
    }

    public void getNameAtLogin() {
        for (int i = 0; i < server.getList().size(); i++) {
            this.name = "Player-" + i;
        }
    }

    public void send(String toSend) {
        out.println(toSend);
    }

    public void incrementScore(int points) {
        score += points;
    }

    public int getScore() {
        return score;
    }

    public Server getServer() {
        return server;
    }
}

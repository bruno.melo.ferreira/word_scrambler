package org.academiadecodigo.tailormoons.wordscrambler.server.commands;

import org.academiadecodigo.tailormoons.wordscrambler.server.ClientHandler;
import org.academiadecodigo.tailormoons.wordscrambler.server.Server;

public class WhisperHandler implements CommandHandler {

    private ClientHandler clientHandler;
    private Server server;
    private String message;


    @Override
    public void execute(ClientHandler clientHandler, Server server, String message) {
        this.clientHandler = clientHandler;
        this.server = server;
        this.message = message;

        String[] words = message.split(" ");
        String command = words[0].toLowerCase();

        if (command.equals("/whisper")) {
            if (words.length > 2) {
                String destination = words[1];
                String messageToSend = "";

                for (int i = 2; i < words.length; i++) {
                    messageToSend += (words[i] + " ");
                }

                for (ClientHandler client : server.getList()) {
                    if (client.getName().equals(destination)) {
                        server.alert(clientHandler.getName() + " (whispered): " + messageToSend, client);
                        server.alert("You whispered to " + client.getName() + ": " + messageToSend, clientHandler);
                    }
                }

            } else {
                server.alert("Wrong use of 'whisper' command. Try '/whisper (space) UserName (space) message you want to send'.", clientHandler);
            }
        }
    }
}

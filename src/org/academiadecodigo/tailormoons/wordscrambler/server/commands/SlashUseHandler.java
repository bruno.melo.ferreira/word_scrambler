package org.academiadecodigo.tailormoons.wordscrambler.server.commands;

import org.academiadecodigo.tailormoons.wordscrambler.server.ClientHandler;
import org.academiadecodigo.tailormoons.wordscrambler.server.Server;

public class SlashUseHandler implements CommandHandler{

    private ClientHandler clientHandler;
    private Server server;
    private String message;

    @Override
    public void execute(ClientHandler clientHandler, Server server, String message) {
        this.clientHandler = clientHandler;
        this.server = server;
        this.message = message;

        server.alert("The use of '/' (slash) is reserved for commands.\nYou can only use it with a command. Messages starting with a slash will not be delivered.", clientHandler);
    }
}

package org.academiadecodigo.tailormoons.wordscrambler.server.commands;

import org.academiadecodigo.tailormoons.wordscrambler.server.ClientHandler;
import org.academiadecodigo.tailormoons.wordscrambler.server.Server;

public class OnlineListHandler implements CommandHandler {

    private ClientHandler clientHandler;
    private Server server;
    private String message;


    @Override
    public void execute(ClientHandler clientHandler, Server server, String message) {
        this.clientHandler = clientHandler;
        this.server = server;
        this.message = message;

        String[] words = message.split(" ");
        String command = words[0].toLowerCase();

        if (command.equals("/list")) {
            if (words.length == 1) {
                    server.alert("---------------------\nLIST OF ONLINE USERS: ", clientHandler);
                for (ClientHandler client : server.getList()) {
                    server.alert(client.getName(), clientHandler);
                }
                    server.alert("---------------------", clientHandler);
            } else {
                server.alert("Use '/list' to get a list of all online users.", clientHandler);
            }

        }
    }
}
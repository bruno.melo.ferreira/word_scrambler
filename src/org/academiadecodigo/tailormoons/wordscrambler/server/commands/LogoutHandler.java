package org.academiadecodigo.tailormoons.wordscrambler.server.commands;

import org.academiadecodigo.tailormoons.wordscrambler.server.ClientHandler;
import org.academiadecodigo.tailormoons.wordscrambler.server.Server;

import java.io.IOException;

public class LogoutHandler implements CommandHandler {

    private ClientHandler clientHandler;
    private Server server;
    private String message;

    @Override
    public void execute(ClientHandler clientHandler, Server server, String message) {
        this.clientHandler = clientHandler;
        this.server = server;
        this.message = message;

        String[] words = message.split(" ");
        String command = words[0].toLowerCase();

        if (command.equals("/logout")) {
            if (words.length == 1) {
                server.broadcast(clientHandler.getName() + " just left the conversation.", clientHandler);
                try {
                    server.getList().remove(clientHandler);
                    clientHandler.getSocket().shutdownInput();
                    clientHandler.getSocket().shutdownOutput();
                    clientHandler.getSocket().close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }
}

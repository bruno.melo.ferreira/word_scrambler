package org.academiadecodigo.tailormoons.wordscrambler.server;

import org.academiadecodigo.tailormoons.wordscrambler.scrambler.Scrambler;
import org.academiadecodigo.tailormoons.wordscrambler.scrambler.Timer;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Server implements Runnable {

    private ServerSocket server;
    private Socket clientSocket;
    private LinkedList<ClientHandler> list = new LinkedList<>();
    private Scrambler scrambler;
    private Timer timer;

    public Server(int port) {
        try {
            server = new ServerSocket(port);
        } catch (IOException exception) {
            exception.printStackTrace();
        }

    }


    public LinkedList<ClientHandler> getList() {
        return list;
    }

    public Scrambler getScrambler() {
        return scrambler;
    }

    public void run() {
        ExecutorService fixedPool = Executors.newFixedThreadPool(500);
        scrambler = new Scrambler();
        timer = new Timer();

        while (!server.isClosed()) {
            try {
                clientSocket = server.accept();
            } catch (IOException exception) {
                exception.printStackTrace();
            }

            ClientHandler client = new ClientHandler(clientSocket, this);
            list.add(client);
            fixedPool.execute(client);
        }
    }

    public void broadcast(String toSend, ClientHandler clientHandler) {

        for (ClientHandler client : list) {
            if (client != clientHandler) {
                client.send(toSend);
            }
        }
    }

    public void alert(String alert, ClientHandler client) {
        client.send(alert);
    }

    public Timer getTimer() {
        return timer;
    }
}
